import { history } from '../history';
import { signinApi, userApi } from '../services/signin';
import { userConstants } from '../constants/user';
import { alertActions } from '../actions/alert';
import { useDispatch, useSelector } from 'react-redux';


// export const userActions = {
//   signin,
//   signout
// };


export function user(user) {



    return dispatch => {
        return userApi(user)
            .then(user => {
                dispatch(success(user));
                console.log("user success" + user.status);
                console.log("user success" + user);
            },
                error => {
                    //dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    //function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
};


async function fetchUsers() {


    const response = await fetch("http://localhost:9000/digihealth/users")



    return response.json()
}

export const getAllUsers = () => {
    return dispatch => {
        fetchUsers().then(response => {
            dispatch({
                type: userConstants.GETALL_SUCCESS,
                users: response.response
            })
        }).catch(err => {
            dispatch({
                type: "GET_ALL_FAILURE"
            })
        })
    }
}
