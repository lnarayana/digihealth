import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { useDispatch, useSelector } from "react-redux";
import { user } from "../../actions/user";
import "./Patient.css";

// const formItems = [
//     {
//         type: "text",
//         dataKey: "firstName",
//         className: "form-control"
//     },
//     {
//         type: "text",
//         dataKey: "lastName",
//         className: "form-control"
//     },
//     {
//         type: "date",
//         dataKey: "dateOfBirth",
//         className: "form-control date-pickee"

//     },

// ];

const Patient = (props) => {
    // const history = useHistory();

    // const users = useSelector(state => state.users)

    const [formData, setFormData] = useState({});

    const handleChange = (key, value) => {
        setFormData({
            ...formData,
            [key]: value,
        });
    };

    console.log("new form data is ", formData);
    const onSubmit = (e) => {
        const user = {
            emailId: formData.email,
            password: "Q!w2e3r4",
            firstName: formData.firstName,
            middleName: formData.middleName,
            lastName: formData.lastName,
            activate: true,
            dateOfBirth: null,
            maritalStatus: true,
            phoneNumber: formData.phoneNumber,
            disease: formData.disease,
            country: formData.country,
            address: formData.address,
            city: formData.city,
            state: formData.state,
            zip: formData.postal,
            comments: formData.comments
        };
        e.preventDefault();
        props
            .userreg(user)
            .then(() => { })
            .catch((err) => {
                console.log(err);
            });
    };

    return (
        <div className="patient">
            <div className="patient__container">
                <h1>Patient Details</h1>
                <form name="form">
                    <h4>Patient Name *</h4>
                    <div className="patient__name__container">
                        <div className={"form-group" + (!formData.firstName ? " has-error" : "")}>
                            <label htmlFor="firstname">First Name</label>
                            {/* <input type="text" className="form-control" name="firstname" value={firstname} onChange={(e) => setFirstName(e.target.value)} /> */}

                            <input
                                type="text"
                                className="form-control"
                                name="firstname"
                                value={formData && formData.firstName ? formData.firstName : ""}
                                onChange={(e) => handleChange("firstName", e.target.value)}
                            />
                        </div>
                        <div className={"form-group" + (!formData.middleName ? " has-error" : "")}>
                            <label htmlFor="middlename">Middle Name</label>
                            <input
                                type="text"
                                className="form-control"
                                name="middleName"
                                value={formData && formData.middleName ? formData.middleName : ""}
                                onChange={(e) => handleChange("middleName", e.target.value)}
                            />
                        </div>
                        <div className={"form-group" + (!formData.lastName ? " has-error" : "")}>
                            <label htmlFor="lastname">Last Name</label>
                            <input
                                type="text"
                                className="form-control"
                                name="lastName"
                                value={formData && formData.lastName ? formData.lastName : ""}
                                onChange={(e) => handleChange("lastName", e.target.value)}
                            />
                        </div>
                    </div>

                    <h4>Address *</h4>
                    <div className="patient__address__container">
                        <div className={"form-group" + (!formData.address ? " has-error" : "")}>
                            <label htmlFor="addressline">Address Line</label>
                            <input
                                type="text"
                                className="form-control"
                                name="address"
                                value={formData && formData.address ? formData.address : ""}
                                onChange={(e) => handleChange("address", e.target.value)}
                            />
                        </div>
                        <div className={"form-group" + (!formData.city ? " has-error" : "")}>
                            <label htmlFor="city">City</label>
                            <input
                                type="text"
                                className="form-control"
                                name="city"
                                value={formData && formData.city ? formData.city : ""}
                                onChange={(e) => handleChange("city", e.target.value)}
                            />
                        </div>
                        <div className={"form-group" + (!formData.state ? " has-error" : "")}>
                            <label htmlFor="state">State</label>
                            <input
                                type="text"
                                className="form-control"
                                name="state"
                                value={formData && formData.state ? formData.state : ""}
                                onChange={(e) => handleChange("state", e.target.value)}
                            />
                        </div>
                        <div className={"form-group" + (!formData.postal ? " has-error" : "")}>
                            <label htmlFor="postal">Postal/Zipcode</label>
                            <input
                                type="text"
                                className="form-control"
                                name="postal"
                                value={formData && formData.postal ? formData.postal : ""}
                                onChange={(e) => handleChange("postal", e.target.value)}
                            />
                        </div>
                    </div>

                    <h4>Personal Details *</h4>
                    <div className="personal__details__container">
                        <div className={"form-group" + (!formData.email ? " has-error" : "")}>
                            <label htmlFor="email">Email</label>
                            <input
                                type="text"
                                className="form-control"
                                name="email"
                                value={formData && formData.email ? formData.email : ""}
                                onChange={(e) => handleChange("email", e.target.value)}
                            />
                        </div>
                        <div className={"form-group" + (!formData.phoneNumber ? " has-error" : "")}>
                            <label htmlFor="phonenumber">Mobile</label>
                            <input
                                type="text"
                                className="form-control"
                                name="phonenumnber"
                                value={formData && formData.phoneNumber ? formData.phoneNumber : ""}
                                onChange={(e) => handleChange("phonenumber", e.target.value)}
                            />
                        </div>
                        <div className={"form-group" + (!formData.disease ? " has-error" : "")}>
                            <label htmlFor="disease">Disease</label>
                            <input
                                type="text"
                                className="form-control"
                                name="disease"
                                value={formData && formData.disease ? formData.disease : ""}
                                onChange={(e) => handleChange("disease", e.target.value)}
                            />
                        </div>
                        <div className={"form-group" + (!formData.dob ? " has-error" : "")}>
                            <label htmlFor="dob">Date Of Birth</label>
                            <input
                                type="text"
                                className="form-control"
                                name="dob"
                                value={formData && formData.dob ? formData.dob : ""}
                                onChange={(e) => handleChange("dob", e.target.value)}
                            />
                        </div>
                        <div
                            className={"form-group" + (!formData.maritalstatus ? " has-error" : "")}
                        >
                            <label htmlFor="maritalstatus">Marital Status</label>
                            <input
                                type="text"
                                className="form-control"
                                name="maritalstatus"
                                value={formData && formData.maritalstatus ? formData.maritalstatus : ""}
                                onChange={(e) => handleChange("matitalStatus", e.target.value)}
                            />
                        </div>
                        <div className={"form-group" + (!formData.comments ? " has-error" : "")}>
                            <label htmlFor="comments">Comments</label>
                            <input
                                type="text"
                                className="form-control"
                                name="comments"
                                value={formData && formData.comments ? formData.comments : ""}
                                onChange={(e) => handleChange("comments", e.target.value)}
                            />
                        </div>
                    </div>

                    <div className="form-group">
                        <button className="btn btn-primary" onClick={onSubmit}>
                            Submit
            </button>
                    </div>
                </form>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => {
    const { user } = state.user;
    return { user };
};

const mapDispatchToProps = (dispatch) => {
    return {
        userreg: bindActionCreators(user, dispatch),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Patient);
