import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { getAllUsers } from '../../actions/user'
import "./Home.css";

class Home extends React.Component {
    componentDidMount = () => {
        this.props.getAllUsers()
    }
    render() {
        const { allUsers = [] } = this.props
        return (
            <div className="home">
                {allUsers.map((user, userIndex) => {
                    return (

                        <>

                            <p>#{userIndex}</p>
                            <h3>
                                {user.firstName}
                            </h3>
                        </>
                    )
                })}

                <h1 style={{ color: "red" }}> Home </h1>
            </div>

        )
    }

}


const stateMappers = (state) => {
    return {
        allUsers: state.allUsers.items
    }
}



const dispatchMapper = (dispatch) => {
    return {
        getAllUsers: bindActionCreators(getAllUsers, dispatch)
    }
}


export default connect(stateMappers, dispatchMapper)(Home);