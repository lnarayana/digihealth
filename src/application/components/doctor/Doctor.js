import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { useDispatch, useSelector } from "react-redux";
import { user } from "../../actions/user";
import "./Doctor.css";

// const formItems = [
//     {
//         type: "text",
//         dataKey: "firstName",
//         className: "form-control"
//     },
//     {
//         type: "text",
//         dataKey: "lastName",
//         className: "form-control"
//     },
//     {
//         type: "date",
//         dataKey: "dateOfBirth",
//         className: "form-control date-pickee"

//     },

// ];

const Doctor = (props) => {
    // const history = useHistory();

    // const users = useSelector(state => state.users)

    const [formData, setFormData] = useState({});

    const handleChange = (key, value) => {
        setFormData({
            ...formData,
            [key]: value,
        });
    };

    console.log("new form data is ", formData);
    const onSubmit = (e) => {
        const user = {
            emailId: formData.email,
            password: "Q!w2e3r4",
            firstName: formData.firstName,
            middleName: formData.middleName,
            lastName: formData.lastName,
            activate: true,
            dateOfBirth: null,
            maritalStatus: true,
            phoneNumber: formData.phoneNumber,
            disease: formData.disease,
            country: formData.country,
            address: formData.address,
            city: formData.city,
            state: formData.state,
            zip: formData.postal,
            comments: formData.comments
        };
        e.preventDefault();
        props
            .userreg(user)
            .then(() => { })
            .catch((err) => {
                console.log(err);
            });
    };

    return (
        <div className="doctor">
            <div className="doctor__container">
                <h1>Doctor Details</h1>
                <form name="form">
                    <h4>Doctor Name *</h4>
                    <div className="doctor__name__container">
                        <div className={"form-group" + (!formData.specilization ? " has-error" : "")}>
                            <label htmlFor="specilization">Specilization</label>
                            {/* <input type="text" className="form-control" name="firstname" value={firstname} onChange={(e) => setFirstName(e.target.value)} /> */}

                            <input
                                type="text"
                                className="form-control"
                                name="specilization"
                                value={formData && formData.specilization ? formData.specilization : ""}
                                onChange={(e) => handleChange("specilization", e.target.value)}
                            />
                        </div>
                        <div className={"form-group" + (!formData.doctorName ? " has-error" : "")}>
                            <label htmlFor="doctorName">Doctor Name</label>
                            <input
                                type="text"
                                className="form-control"
                                name="doctorName"
                                value={formData && formData.doctorName ? formData.doctorName : ""}
                                onChange={(e) => handleChange("doctorName", e.target.value)}
                            />
                        </div>
                    </div>

                    <h4>Address *</h4>
                    <div className="doctor__address__container">
                        <div className={"form-group" + (!formData.address ? " has-error" : "")}>
                            <label htmlFor="address">Address </label>
                            <input
                                type="text"
                                className="form-control"
                                name="address"
                                value={formData && formData.address ? formData.address : ""}
                                onChange={(e) => handleChange("address", e.target.value)}
                            />
                        </div>
                        <div className={"form-group" + (!formData.contactno ? " has-error" : "")}>
                            <label htmlFor="contactno">Mobile</label>
                            <input
                                type="text"
                                className="form-control"
                                name="contactno"
                                value={formData && formData.contactno ? formData.contactno : ""}
                                onChange={(e) => handleChange("contactno", e.target.value)}
                            />
                        </div>
                    </div>

                    <h4>Personal Details *</h4>
                    <div className="doctor__details__container">
                        <div className={"form-group" + (!formData.email ? " has-error" : "")}>
                            <label htmlFor="email">Email</label>
                            <input
                                type="text"
                                className="form-control"
                                name="email"
                                value={formData && formData.email ? formData.email : ""}
                                onChange={(e) => handleChange("email", e.target.value)}
                            />
                        </div>
                    </div>

                    <div className="form-group">
                        <button className="btn btn-primary" onClick={onSubmit}>
                            Submit
            </button>
                    </div>
                </form>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => {
    const { user } = state.user;
    return { user };
};

const mapDispatchToProps = (dispatch) => {
    return {
        userreg: bindActionCreators(user, dispatch),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Doctor);
