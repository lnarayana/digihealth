import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { useDispatch, useSelector } from "react-redux";
import { user } from "../../actions/user";
import "./Appointment.css";

// const formItems = [
//     {
//         type: "text",
//         dataKey: "firstName",
//         className: "form-control"
//     },
//     {
//         type: "text",
//         dataKey: "lastName",
//         className: "form-control"
//     },
//     {
//         type: "date",
//         dataKey: "dateOfBirth",
//         className: "form-control date-pickee"

//     },

// ];

const Appointment = (props) => {
    // const history = useHistory();

    // const users = useSelector(state => state.users)

    const [formData, setFormData] = useState({});

    const handleChange = (key, value) => {
        setFormData({
            ...formData,
            [key]: value,
        });
    };

    console.log("new form data is ", formData);
    const onSubmit = (e) => {
        const user = {
            emailId: formData.email,
            password: "Q!w2e3r4",
            firstName: formData.firstName,
            middleName: formData.middleName,
            lastName: formData.lastName,
            activate: true,
            dateOfBirth: null,
            maritalStatus: true,
            phoneNumber: formData.phoneNumber,
            disease: formData.disease,
            country: formData.country,
            address: formData.address,
            city: formData.city,
            state: formData.state,
            zip: formData.postal,
            comments: formData.comments
        };
        e.preventDefault();
        props
            .userreg(user)
            .then(() => { })
            .catch((err) => {
                console.log(err);
            });
    };

    return (
        <div className="appointment">
            <div className="appointment__container">
                <h1>Appointment Details</h1>
                <form name="form">
                    <h4>Doctor Name *</h4>
                    <div className="appointment__name__container">
                        <div className={"form-group" + (!formData.userId ? " has-error" : "")}>
                            <label htmlFor="userId">Doctor</label>
                            {/* <input type="text" className="form-control" name="firstname" value={firstname} onChange={(e) => setFirstName(e.target.value)} /> */}

                            <input
                                type="text"
                                className="form-control"
                                name="specilization"
                                value={formData && formData.userId ? formData.userId : ""}
                                onChange={(e) => handleChange("userId", e.target.value)}
                            />
                        </div>
                        <div className={"form-group" + (!formData.appointmentDate ? " has-error" : "")}>
                            <label htmlFor="appointmentdate">Appointment Date</label>
                            <input
                                type="text"
                                className="form-control"
                                name="appointmentdate"
                                value={formData && formData.appointmentdate ? formData.appointmentdate : ""}
                                onChange={(e) => handleChange("appointmentdate", e.target.value)}
                            />
                        </div>
                        <div className={"form-group" + (!formData.appointmentTime ? " has-error" : "")}>
                            <label htmlFor="appointmenttime">Appointment Time</label>
                            <input
                                type="text"
                                className="form-control"
                                name="appointmenttime"
                                value={formData && formData.appointmenttime ? formData.appointmenttime : ""}
                                onChange={(e) => handleChange("appointmenttimes", e.target.value)}
                            />
                        </div>
                    </div>



                    <div className="form-group">
                        <button className="btn btn-primary" onClick={onSubmit}>
                            Submit
            </button>
                    </div>
                </form>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => {
    const { user } = state.user;
    return { user };
};

const mapDispatchToProps = (dispatch) => {
    return {
        userreg: bindActionCreators(user, dispatch),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Appointment);
