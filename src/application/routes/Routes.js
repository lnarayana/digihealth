import React from 'react';
import { Redirect, Route, Switch, useHistory } from "react-router-dom";
import Home from "../components/home/Home";
import SignIn from "../components/signIn/SignIn";
import Patient from "../components/patient/Patient";
import Doctor from "../components/doctor/Doctor";
import Appointment from "../components/appointment/Appointment";
import { store } from '../helper/store';
import { useEffect } from 'react';

const checkIfAuthed = (store) => {

  const state = store.getState();
  console.log(" login status" + state.signin.loggedIn);
  return state.signin.loggedIn;
  //state.signin.loggedIn;
};


const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    checkIfAuthed(store) ? (
      <Component {...props} />
    ) : (
      <Redirect to={{
        pathname: '/login',
        state: { from: props.location }
      }} />
    )
  )} />
)

function Routes() {

  const history = useHistory()

  useEffect(() => {
    const isLoggedIn = checkIfAuthed(store)

    if (isLoggedIn) {
      history.push('/home')
    }
    else {
      history.push('/login')
    }

  }, [])

  return (
    <Switch>
      <Route path="/login" exact component={SignIn} />
      {/* <Redirect exact from="/" to="/login" /> */}
      <PrivateRoute exact path="/home" component={Home} />
      <PrivateRoute exact path="/patient" component={Patient} />
      <PrivateRoute exact path="/doctor" component={Doctor} />
      <PrivateRoute exact path="/appointment" component={Appointment} />
    </Switch>
  )
}

export default Routes;
